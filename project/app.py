import json

from flask import Flask, request, abort, jsonify
from flask_redis import FlaskRedis

app = Flask(__name__)
app.config.from_pyfile('setup.py')

redis_store = FlaskRedis(app)


@app.route('/')
def hello_world():
    return 'Flask is running!'


@app.route('/storeIntoRedis', methods=['POST'])
def storeIntoRedis():
    json_data = request.get_json()
    if not json_data or not 'Timestamp' in json_data:
        abort(400)
    json_data = json.dumps(json_data)
    resp = redis_store.set('req_data', json_data)
    return jsonify(resp)


@app.route('/getFromRedis', methods=['GET'])
def getFromRedis():
    json_data = redis_store.get('req_data')
    return jsonify(json_data)


@app.route('/getTimeStampFromRedis', methods=['GET'])
def getTimeStampFromRedis():
    json_data = redis_store.get('req_data')
    time_stamp = json.loads(json_data).get("Timestamp", None) if json_data else None
    print "Timestamp : %s" % time_stamp
    return jsonify({"Timestamp": time_stamp})


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=5001)
