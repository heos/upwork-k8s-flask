import os

REDIS_HOST = os.environ['REDIS_CLUSTER_SERVICE_HOST'] if os.environ.get('GET_HOSTS_FROM', '') == 'env' else 'redis-master'
REDIS_PORT = 6379
REDIS_URL = "redis://{}:{}/0".format(REDIS_HOST, REDIS_PORT)
