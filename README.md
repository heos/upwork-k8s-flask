# Redis Cluster & FLask on Kubernetes

To build `flask-web` image you have to do:
1. Go to the `project` directory.
2. Run `docker build -t flask-web .`

To build `redis-cluster:v1` image you have to do:
1. Go to the `redis` directory.
2. Run `docker build -t redis-cluster:v1 .`

All these images should be copied to your personal Docker registry or to the all
worker nodes, that will be added to your Kubernets cluster.
